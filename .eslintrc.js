module.exports = {
    parser: 'babel-eslint',
    env: {
      browser: true,
      commonjs: true,
      es6: true,
      node: true,
      jest: true,
      jquery: true
    },
    extends: ['eslint:recommended', 'plugin:react/recommended'],
    parserOptions: {
      ecmaFeatures: {
        experimentalObjectRestSpread: true,
        jsx: true,
      },
      sourceType: 'module',
    },
    plugins: ['react'],
    rules: {
      indent: ['error', 4, { SwitchCase: 1 }],
      'linebreak-style': ['error', 'windows'],
      quotes: ['error', 'single'],
      semi: ['error', 'never'],
      'no-console': ['warn', { allow: ['info', 'error'] }],
      'arrow-parens': ['error', 'always'],
      "react/no-direct-mutation-state": ['error', 'always'],
      "react/no-multi-comp": ["error", { 'ignoreStateless': false }],
      "react/no-unused-prop-types": [2],
      "react/prefer-stateless-function": [1, { "ignorePureComponents": false }],
      "react/prop-types": [2],
      "react/sort-comp": [2],
      "react/no-deprecated": [2],
      "react/sort-prop-types": [2, {
        "callbacksLast": true,
        "ignoreCase": true,
        "requiredFirst": true,
      }],
      "react/jsx-curly-spacing": [2, "always"],
      "react/jsx-equals-spacing": [2, "never"],
      // "react/jsx-handler-names": [1, {
      //   "eventHandlerPrefix": <eventHandlerPrefix>,
      //   "eventHandlerPropPrefix": <eventHandlerPropPrefix>
      // }],
      "react/jsx-max-props-per-line": [2, { "maximum": 3, "when": "always" }],
      "react/jsx-no-duplicate-props": [2, { "ignoreCase": false }]
    },
};