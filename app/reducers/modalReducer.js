import * as types from '../actions/actionTypes'

let initialState = {
    addUser: false,
    deleteUser: false,
}

export default function modalReducer( state = initialState, action ) {
    switch ( action.type ) {
        case types.SHOW_ADD_USER_MODAL:
            return Object.assign( {}, initialState, { addUser: true } )
        case types.SHOW_DELETE_USER_MODAL:
            return Object.assign( {}, initialState, { deleteUser: true } )
        case types.ADD_USER_SUCCESS:
            return Object.assign( {}, initialState )
        case types.DELETE_USER_SUCCESS:
            return Object.assign( {}, initialState )
        case types.HIDE_MODALS:
            return initialState
        default:
            return state
    }
}


