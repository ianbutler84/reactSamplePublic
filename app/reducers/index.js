// Root Reducer

import { combineReducers } from 'redux'

import usersReducer from './users/usersReducer'
import userReducer from './users/userReducer'
import formatUsersReducer from './users/formatUsersReducer'
import selectedUsersReducer from './users/selectedUsersReducer'
import modalReducer from './modalReducer'

const rootReducer = combineReducers({
    usersReducer, // plural
    userReducer,  // singular
    selectedUsersReducer,
    formatUsersReducer,
    modalReducer
})

export default rootReducer