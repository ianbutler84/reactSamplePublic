import * as types from '../../actions/actionTypes'

let initialState = {
    users: [],
    searchQuery: "",
    lastSearch: ""
}

export default function usersReducer( state = initialState, action ) {
    switch ( action.type ) {
        case types.GET_USERS_SUCCESS:
            return Object.assign( {}, state, {users:action.users, lastSearch: "", searchQuery: ""} )
        case types.SEARCH_USERS_QUERY:
            return Object.assign( {}, state, { searchQuery: action.value })
        case types.SEARCH_USERS_SUCCESS:
            return Object.assign( {}, state, { users: action.users, lastSearch: action.query })
        case types.CLEAR_USER_STATE:
            return Object.assign( {}, initialState )
        default:
            return state
    }
}