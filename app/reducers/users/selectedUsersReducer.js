import * as types from '../../actions/actionTypes'

let initialState = {
    selectedUsers: [],
    allSelected: false
}



export default function selectedUsersReducer( state = initialState, action ) {
    switch ( action.type ) {
        case types.SELECT_USER:
            return selectUserFromCheckbox( action.user, state )
        case types.SELECT_ALL_USERS:
            return selectAllUsers( action.allSelected, action.users, action.activePage, action.amountShown, state )
        case types.GET_USERS_SUCCESS:
            return Object.assign( {}, initialState )
        case types.SEARCH_USERS_SUCCESS:
            return Object.assign( {}, initialState )
        case types.ADD_USER_SUCCESS:
            return Object.assign( {}, initialState )
        case types.EDIT_USER_SUCCESS:
            return Object.assign( {}, initialState )
        case types.DELETE_USER_SUCCESS:
            return Object.assign( {}, initialState )
        case types.RESET_USER_PASSWORD_SUCCESS:
            return Object.assign( {}, initialState )
        case types.SORT_USER_TABLE:
        case types.FILTER_USERS:
            return Object.assign( {}, initialState )
        case types.CLEAR_USER_STATE:
            return Object.assign( {}, initialState )
        default:
            return state
    }
}


// /**
//  * helper functions below
//  * 
//  */

function selectAllUsers ( allSelected, users, activePage, amountShown, state) {
    if (allSelected) {
        return Object.assign( {}, state, { allSelected: false, selectedUsers: [] } )
    } else {
        let initialShown = (activePage - 1) * amountShown
        let lastShown = activePage * amountShown
        return Object.assign( 
            {}, 
            state, 
            { 
                allSelected: true, 
                selectedUsers: [ ...users.slice(initialShown, lastShown) ] 
            } )
    }
}

function selectUserFromCheckbox( selectedUser, state ){
    let userPresent = false, index
    state.selectedUsers.forEach((user, i) => {
        if (user.email == selectedUser.email) {
            userPresent = true
            index = i
        }
    })
    if (userPresent) {
        return Object.assign(
            {}, 
            state, 
            { allSelected: false, selectedUsers: [ ...state.selectedUsers.slice(0, index), ...state.selectedUsers.slice(index + 1) ] }
        )
    }
    else {
        return Object.assign(
            {},
            state,
            { allSelected: false, selectedUsers: [ ...state.selectedUsers, selectedUser ] }
        )
    }
}