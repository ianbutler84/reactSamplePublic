import moment from 'moment'

import * as types from '../../actions/actionTypes'

let initialState = {
    activePage: 1,
    amountShown: 10,
    formattedUsers: [],
    currentFilters: {
        status: ''
    },
    sorted: { param: 'All', direction: 'down'}
}

export default function formatUsersReducer( state = initialState, action ) {
    switch ( action.type ) {
        case types.INITIALIZE_USER_INITIAL_STATE:
            return Object.assign({}, state, { currentFilters: { status: 'All' }})
        case types.HANDLE_USER_PAGE_SELECT:
            return Object.assign( {}, state, {activePage: action.newPage } )
        case types.FORMAT_USERS:
            return formatUsers(action.users, state)
        case types.SELECT_USER:
            return selectUserFromCheckbox( action.user, state )
        case types.SEARCH_USERS_SUCCESS:
            return Object.assign( {}, state, { activePage: 1 })
        case types.SELECT_ALL_USERS:
            return selectAllUsers( action.allSelected, action.users, action.activePage, action.amountShown, state )
        case types.SORT_USER_TABLE:
            return sortUsers(action.param, state)
        case types.FILTER_USERS:
            return filterUsers( action.users, action.filterValue.toLowerCase(), action.filterName, state )
        case types.CLEAR_USER_STATE:
            return Object.assign( {}, initialState )
        default:
            return state
    }
}


/**
 * helper functions below
 * 
 */

function selectAllUsers( allSelected, users, activePage, amountShown, state ) {
    let arr = []
    let initialShown = (activePage - 1) * amountShown
    let lastShown = activePage * amountShown
    if ( allSelected ) {
        state.formattedUsers.forEach( (user, i) => {
            arr.push(Object.assign({}, user, {selected: false}))
        } )
    } else {
        state.formattedUsers.forEach( (user, i) => {
            if ( i >= initialShown && i <= lastShown ) arr.push(Object.assign({}, user, {selected: true}))    
            else arr.push(Object.assign({}, user, {selected: false}))
        } )
    }
    return Object.assign( {}, state, { formattedUsers: arr } )
}

function formatUsers(usersArr, state) {
    let users = []

    usersArr.forEach((user, index) => {
        users.push({
            originalData: user, // this data goes along as a reference for later
            checkbox: true, 
            firstName: user.firstName,
            lastName: user.lastName,
            dateAdded: moment(user.created).format("M/D/YYYY"),
            email: user.email,
            status: user.status,
            role: {
                custom: "customComponent",
                name: "user-role",
                data: {
                    "data-original-data": user.allRoles
                }
            },
            selected: false
        })
    })
    return Object.assign( {}, state, { formattedUsers: users } )
}

function selectUserFromCheckbox( changedUser, state ){
    let index, bool
    state.formattedUsers.forEach((user, i) => {
        if (user == changedUser) {
            bool = user.selected == true ? false : true
            index = i
        }
    })
    return Object.assign({}, state, {formattedUsers: [ ...state.formattedUsers.slice(0, index), 
        Object.assign({}, changedUser, {selected: bool}), 
        ...state.formattedUsers.slice(index + 1) ]}
    )
}

function sortUsers( param, state ) {
    let formattedUsersCopy = state.formattedUsers.slice()
    let formattedUsers 
    let sorted = decideParam( param, state )
    let sortParam

    if ( sorted.param == 'users.tableheader.firstname' ) sortParam = 'firstName'
    else if ( sorted.param == 'users.tableheader.lastname' ) sortParam = 'lastName'
    else if ( sorted.param == 'users.tableheader.dateAdded' ) sortParam = 'dateAdded'
    else if ( sorted.param == 'users.tableheader.emailaddress' ) sortParam = 'email'

    formattedUsers = formattedUsersCopy.sort((a,b) => {
        let sortIndex = findMismatchingDigit(a[sortParam], b[sortParam])
        let aSortAlt = isNaN(a[sortParam].toLowerCase().charCodeAt(sortIndex)) ? '999999999' : a[sortParam].toLowerCase().charCodeAt(sortIndex)
        let bSortAlt = isNaN(b[sortParam].toLowerCase().charCodeAt(sortIndex)) ? '999999999' : b[sortParam].toLowerCase().charCodeAt(sortIndex)
        if (sorted.direction == 'up') {
            if (sortParam == 'dateAdded') return new Date(b[sortParam]).getTime() - new Date(a[sortParam]).getTime()
            else return bSortAlt - aSortAlt
        } else if ( sorted.direction == 'down' ) {
            if (sortParam == 'dateAdded') return new Date(a[sortParam]).getTime() - new Date(b[sortParam]).getTime()
            else return aSortAlt - bSortAlt
        }
    })
    
    let newState = Object.assign( {}, state, { sorted, formattedUsers, activePage:1 } )
    return selectAllUsers( true, [], newState.activePage, newState.amountShown, newState)
}

function findMismatchingDigit(str1, str2) {
    let len = str1.length > str2.length ? str1.length : str2.length
    for ( let i = 0; i < len; i++ ) {
        if (str1[i] == undefined || str2[i] == undefined || str1[i].toLowerCase() != str2[i].toLowerCase()) return i
    }
}

function decideParam( param, state ) {
    let direction
    let newParam = param
    if ( state.sorted.direction == null || param !== state.sorted.param ) {
        direction = 'down'
    } else if ( state.sorted.direction == 'up' ) {
        direction = 'down'
    } else if ( state.sorted.direction == 'down' ) {
        direction = 'up'
    }

    return { param: newParam, direction: direction }
}

function filterUsers( users, filterName, filterValue, state ) {
    let newUsers
    let filterNameUse = filterName == 'users.tableheader.role' ? 'role' : 'status'
    let newFilters = Object.assign({}, state.currentFilters, {[ filterNameUse ]:filterValue})
    let statusComparison

    if ( newFilters.status == 'users.tablefilter.inactive' ) statusComparison = 'inactive'
    else if (newFilters.status == 'users.tablefilter.active') statusComparison = 'active'
    else statusComparison = 'all'

    newUsers = users.filter( (user, i) => {
        let condition1 = statusComparison == user.status.toLowerCase()
        let condition2 = newFilters.status == 'users.tablefilter.all'

        if (condition1 || condition2) {        
            return user
        }
    })

    let newState = Object.assign({}, state, {currentFilters:newFilters, activePage:1} )
    return formatUsers( newUsers, newState )
}
