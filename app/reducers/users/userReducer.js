import * as types from '../../actions/actionTypes'

let initialState = {
    newUserEmail: '',
    newUserFirstName: '',
    newUserLastName: '',
    newUserId: '',
    newUserStatus: ''
}

export default function userReducer( state = initialState, action ) {
    switch ( action.type ) {
        case types.POPULATE_USER_DATA:
            return Object.assign( {}, { newUserEmail: action.user.email, 
                                        newUserFirstName: action.user.originalData.firstName, 
                                        newUserLastName: action.user.originalData.lastName, 
                                        newUserId: action.user.originalData.userID, 
                                        newUserStatus: action.user.status,
                                    })
        case types.EMPTY_USER_DATA:
            return Object.assign( {}, initialState )
        case types.ADD_USER_SUCCESS:
            return Object.assign( {}, initialState )
        case types.DELETE_USER_SUCCESS:
            return Object.assign( {}, initialState )
        case types.HANDLE_USER_CHANGE:
            return Object.assign( {}, state, { [action.key]: action.value })
        case types.CLEAR_USER_STATE:
            return Object.assign( {}, initialState )
        default:
            return state
    }
}