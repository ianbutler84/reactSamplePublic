import { createStore, applyMiddleware, compose } from 'redux'
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant'
import thunk from 'redux-thunk'
import { persistState } from 'redux-devtools'

import rootReducer from '../reducers'
import DevTools from '../components/DevTools/DevTools'


export default function configureStore(initialState) {
    // if dev env... add dev tools
    if (module.hot) {
        const enhancer = compose(
            //applyMiddleware( thunk, reduxImmutableStateInvariant() ),
            applyMiddleware( thunk ),
            DevTools.instrument(),
            persistState(window.location.href.match(/[?&]debug_session=([^&#]+)\b/))
        )
        return createStore(
            rootReducer,
            initialState,
            enhancer
        )
    } 
    // if prod... don't add dev tools
    else {
        return createStore(
            rootReducer,
            initialState,
            //applyMiddleware( thunk, reduxImmutableStateInvariant() )
            applyMiddleware( thunk )
        )
    }
}