import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as userActions from '../../../actions/userActions'
import UsersTableView from './UsersTableView'

class UsersTable extends React.Component {
    constructor(props){
        super(props)

        this.toggleAllItems =           this.toggleAllItems.bind(this)
        this.usersShowing =             this.usersShowing.bind(this)
        this.handlePageSelect =         this.handlePageSelect.bind(this)
        this.sortTable =                this.sortTable.bind(this)
        this.selectFilter =             this.selectFilter.bind(this)
    }
    
    componentDidMount(){
        this.getUsers()
        this.props.actions.initUserState('All')
    }

    componentWillUnmount() {
        this.props.actions.clearUserState()
    }

    getUsers(){
        this.props.actions.getUsers()
    }

    usersShowing() {
        let activePage = this.props.formattedUsers.activePage
        let amountShown = this.props.formattedUsers.amountShown
        let initialShown = (activePage - 1) * amountShown
        let lastShown = activePage * amountShown
        
        return this.props.formattedUsers.formattedUsers.slice( initialShown, lastShown ).length
    }

    toggleAllItems(){
        this.props.actions.selectAllUsers( this.props.selectedUsers.allSelected, this.props.formattedUsers.formattedUsers, this.props.formattedUsers.activePage, this.props.formattedUsers.amountShown )
    }

    // handles the paginator selection
    handlePageSelect(eventKey){
        this.props.actions.handleUserPageSelect(eventKey)
        this.props.actions.selectAllUsers( true, this.props.formattedUsers.formattedUsers, this.props.formattedUsers.activePage, this.props.formattedUsers.amountShown )
    }

    sortTable( param ) {
        this.props.actions.sortUsersTable(param)
    }

    selectFilter(filterValue, filterName){
        this.props.actions.filterUsers( this.props.users.users, filterValue, filterName )
    }

    render() {
        const { translate } = this.props
        let headings = [
            'checkbox', 
            { name: 'First name', sortable: true }, 
            { name: 'Last name', sortable: true }, 
            { name: 'Added', sortable: true }, 
            'Email address',
            { name: 'Status', filterable: true, options: ['All', 'Active', 'Inactive'], current: this.props.formattedUsers.currentFilters.status }
        ]

        return (
            <UsersTableView 
                selectedUsers={ this.props.selectedUsers.selectedUsers }
                checkItem={ this.props.actions.selectUser }
                allSelected={ this.props.selectedUsers.allSelected }
                selectAll={ this.toggleAllItems }
                headings={ headings }
                usersShowing={ this.usersShowing }
                activePage={ this.props.formattedUsers.activePage }
                amountShown={ this.props.formattedUsers.amountShown }
                handlePageSelect={ this.handlePageSelect }
                body={ this.props.formattedUsers.formattedUsers }
                sorter={ this.sortTable }
                sortDirection={ this.props.formattedUsers.sorted.direction }
                sortParam={ this.props.formattedUsers.sorted.param }
                selectFilter={ this.selectFilter }
            />
        )
    }

}

function mapStateToProps( state, ownProps ) {
    return {
        users:          state.usersReducer, // plural
        formattedUsers: state.formatUsersReducer,
        selectedUsers:  state.selectedUsersReducer
    }
}

function mapDispatchToProps( dispatch ) {
    return {
        actions: bindActionCreators( userActions, dispatch )
    }
}

export default connect( mapStateToProps, mapDispatchToProps )( UsersTable )
