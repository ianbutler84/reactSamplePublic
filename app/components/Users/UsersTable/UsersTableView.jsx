import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-bootstrap'

import Paginator from '../../Common/Paginator'
import CommonTable from '../../Common/TableElements/CommonTable'
import DisplayRole from '../../Common/TableElements/CustomTDs/DisplayRole'
import { FlexJustAlignCenter } from '../../Common/CommonStyle'

function UsersTableView ({ 
    selectedUsers, 
    body, 
    selectAll, 
    checkItem, 
    allSelected, 
    activePage, 
    amountShown, 
    usersShowing, 
    headings, 
    handlePageSelect, 
    sorter, 
    sortDirection, 
    sortParam,
    translate,
    selectFilter 
}) {
    
    let initialShown = (activePage - 1) * amountShown
    let lastShown = activePage * amountShown
    let selectedMessage = `${selectedUsers.length} users selected`

    return (
        <Row>
            <Col style={ {paddingTop: '15px', position: 'relative'} } lg={ 12 }>
                { 
                    selectedUsers.length > 0 
                    && <p style={ { position: 'absolute', top: '-10px', left: '15px' } } className='text-info'>{ selectedMessage }</p> 
                }
                <CommonTable 
                    sorter={ sorter }
                    checkItem={ checkItem } 
                    allItemsSelected={ allSelected } 
                    selectAll={ selectAll } 
                    body={ body.slice( initialShown, lastShown ) } 
                    headings={ headings } 
                    sortDirection={ sortDirection }
                    sortParam={ sortParam }
                    selectFilter={ selectFilter }
                    translate={ translate }
                >
                    {
                        {
                            // 'user-role' data is added in the reducer
                            'user-role': <DisplayRole />
                        }
                    }
                </CommonTable>
                <p className="pull-right">{ `Showing ${usersShowing()} of ${body.length} users` }</p>
                <FlexJustAlignCenter>
                    <Paginator 
                        length={ body.length } 
                        amountShown={ amountShown } 
                        activePage={ activePage } 
                        handlePageSelect={ handlePageSelect } 
                    />
                </FlexJustAlignCenter>
            </Col>
        </Row>
    )
}

UsersTableView.propTypes = {
    activePage:         PropTypes.number.isRequired,
    allSelected:        PropTypes.bool.isRequired,
    amountShown:        PropTypes.number.isRequired,
    body:               PropTypes.array.isRequired,
    checkItem:          PropTypes.func.isRequired,
    handlePageSelect:   PropTypes.func.isRequired,
    headings:           PropTypes.array.isRequired,
    selectAll:          PropTypes.func.isRequired,
    selectedUsers:      PropTypes.array.isRequired,
    selectFilter:       PropTypes.func.isRequired, 
    sortDirection:      PropTypes.string.isRequired, 
    sorter:             PropTypes.func.isRequired, 
    sortParam:          PropTypes.string.isRequired, 
    usersShowing:       PropTypes.func.isRequired,
}


export default UsersTableView