import React from 'react'
import { Row, Col, Panel } from 'react-bootstrap'
import { connect } from 'react-redux'

import CommonPageHeader from '../Common/CommonPageHeader'
import ContentWrapper from '../Layout/ContentWrapper'
import UsersTable from './UsersTable/UsersTable'
import ManageUsers from './ManageUsers/ManageUsers'

function UsersPage({translate}) {
    return (
        <ContentWrapper>
            <CommonPageHeader title="Title of page" description="Description" />
            <Row>
                <Col lg={ 12 }>
                    <Panel header="Title of section" >
                        <ManageUsers />
                        <UsersTable /> 
                    </Panel>
                </Col>
            </Row>
        </ContentWrapper>
    )
}

UsersPage.propTypes = {} 

export default UsersPage 