import React from 'react'
import PropTypes from 'prop-types'
import { Row } from 'react-bootstrap'

import CommonModal from '../../Common/CommonModal'
import AddUser from './Modals/AddUser'
import DeleteUser from './Modals/DeleteUser'


const UserModals = ({
    addUserModalVisible,
    deleteUserModalVisible,
    toggleModal,
    onChange,
    newUserRole,
    newUserFirstName,
    newUserLastName,
    newUserEmail,
    newUserStatus,
    addUser,
    deleteUser,
    userLength,
    selectRadio,
    toggleActivation,
    userActivated,
    addUserErrors
}) => {

    return (
        <div>
            <CommonModal
                modalVisible={ addUserModalVisible }
                toggleModal={ toggleModal }
                titleForModal="Add user"
                component={ 
                    <Row>
                        <AddUser 
                            onChange={ onChange }
                            selectRadio={ selectRadio }
                            newUserRole={ newUserRole }
                            newUserFirstName={ newUserFirstName }
                            newUserLastName={ newUserLastName }
                            newUserEmail={ newUserEmail }
                            onClick={ addUser } 
                            toggleModal={ toggleModal }
                            errors={ addUserErrors }
                        /> 
                    </Row>
                }                     
            />
            <CommonModal
                titleForModal="Delete user"
                modalVisible={ deleteUserModalVisible }
                toggleModal={ toggleModal }
                component={ 
                    <Row>
                        <DeleteUser
                            toggleModal={ toggleModal }
                            onClick={ deleteUser }
                            userLength={ userLength }
                        /> 
                    </Row>
                }                     
            />
        </div>
    )
}

UserModals.propTypes = {
    addUserModalVisible:        PropTypes.bool.isRequired,
    deleteUserModalVisible:     PropTypes.bool.isRequired,
    toggleModal:                PropTypes.func.isRequired,
    onChange:                   PropTypes.func.isRequired,
    newUserRole:                PropTypes.string.isRequired,
    newUserEmail:               PropTypes.string.isRequired,
    addUser:                    PropTypes.func.isRequired,
    newUserFirstName:           PropTypes.string.isRequired,
    newUserLastName:            PropTypes.string.isRequired,
    deleteUser:                 PropTypes.func.isRequired
}

export default UserModals