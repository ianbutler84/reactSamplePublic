import React from 'react'
import PropTypes from 'prop-types'
import { Col } from 'react-bootstrap'

import { FlexJustEnd, ModalBottomButtonGroup } from '../../../Common/CommonStyle'

const DeleteUser = ({ translate, toggleModal, onClick, userLength }) => {
    let plural = userLength > 1 ? `Delete these ${userLength} users` : 'Delete this user'  // add an 's' if more than 1
    
    return (
        <Col lg={ 12 }>
            <p>{ plural }</p>
            <ModalBottomButtonGroup>
                <FlexJustEnd>
                    <button style={ {marginRight: '5px'} } onClick={ onClick } className='btn btn-primary'>Delete</button>
                    <button onClick={ toggleModal } className='btn btn-default'>Cancel</button>
                </FlexJustEnd>
            </ModalBottomButtonGroup>
        </Col>
    )
}

DeleteUser.propTypes = {
    toggleModal:    PropTypes.func.isRequired, 
    onClick:        PropTypes.func.isRequired
}

export default DeleteUser