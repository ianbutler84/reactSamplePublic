import React from 'react'
import PropTypes from 'prop-types'
import { Col } from 'react-bootstrap'

import TextInput from '../../../Common/FormElements/TextInput'
import Radio from '../../../Common/FormElements/Radio'
import CommonInputError from '../../../Common/FormElements/CommonInputError.jsx'

import { FlexJustEnd, FlexAlignCenter, ModalBottomButtonGroup } from '../../../Common/CommonStyle'

const AddUser = ({ translate, toggleModal, onClick, selectRadio, onChange, newUserEmail, newUserRole, newUserFirstName, newUserLastName, errors }) => {

    return (
        <Col lg={ 12 }>
            <form noValidate onSubmit={ onClick }>
                <TextInput label="First name" value={ newUserFirstName } name="newUserFirstName" onChange={ onChange } type="name">
                    <CommonInputError showError={ errors.newUserFirstName.tests.required } errorMessage="Required" />
                </TextInput>
                <TextInput label="Last name" value={ newUserLastName } name="newUserLastName" onChange={ onChange } type="name">
                    <CommonInputError showError={ errors.newUserLastName.tests.required } errorMessage="Required" />
                </TextInput>
                <TextInput label="Email" value={ newUserEmail } name="newUserEmail" onChange={ onChange } type="email">
                    <CommonInputError showError={ errors.newUserEmail.tests.required } errorMessage="Required" />
                    { errors.newUserEmail.tests.required == false && <CommonInputError showError={ errors.newUserEmail.tests.validEmail } errorMessage="Must be a valid email address" />}
                </TextInput>
                <ModalBottomButtonGroup>
                    <FlexJustEnd>
                        <button type="submit" style={ { marginRight: "10px" } } className="btn btn-primary">Add</button>
                        <button onClick={ toggleModal } className="btn btn-default">Cancel</button>
                    </FlexJustEnd>
                </ModalBottomButtonGroup>
            </form>
        </Col>
    )
}

AddUser.propTypes = {
    onChange:           PropTypes.func.isRequired,
    onClick:            PropTypes.func.isRequired,
    newUserEmail:       PropTypes.string.isRequired,
    newUserRole:        PropTypes.string.isRequired,
    toggleModal:        PropTypes.func.isRequired,
    newUserFirstName:   PropTypes.string.isRequired,
    newUserLastName:    PropTypes.string.isRequired
}

export default AddUser