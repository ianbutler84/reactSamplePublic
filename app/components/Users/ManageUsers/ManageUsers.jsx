import React from 'react'
import swal from 'sweetalert'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as userActions from '../../../actions/userActions'
import * as modalActions from '../../../actions/modalActions'
import UserModals from './UserModals'
import ManageUsersView from './ManageUsersView'

class ManageUsers extends React.Component {
    
    constructor(props){
        super(props)

        this.addUser =                  this.addUser.bind(this)
        this.handleTextInputChange =    this.handleTextInputChange.bind(this)
        this.deleteUser =               this.deleteUser.bind(this)
        this.toggleModal =              this.toggleModal.bind(this)
        this.onChange =                 this.onChange.bind(this)
        this.filterUserSearchResult =   this.filterUserSearchResult.bind(this)
        this.getUsers =                 this.getUsers.bind(this)
        this.selectRadio =              this.selectRadio.bind(this)
        this.activateUser =             this.activateUser.bind(this)
    }

    getUsers(){
        this.props.actions.getUsers( )
    }

    addUser(e){
        e.preventDefault()
        const email = this.props.user.newUserEmail.trim()
        this.validateAddUser(this.props.user.newUserFirstName, this.props.user.newUserLastName, email)        
        
        if ( this.props.errors.addUser.invalid) return 
        this.props.actions.addUser( this.props.user )
            .then(() => { 
                swal(this.props.translate('users.modalpopup.addUser.popup.success', { email }))
            })
    }

    validateAddUser( fname, lname, email ) {
        let itemsToCheck = {
            newUserEmail: email,
            newUserFirstName: fname,
            newUserLastName: lname
        }
        this.props.actions.validateAddUser( itemsToCheck )
    }

    // not tested at all... need orgID to be fixed first
    deleteUser() {
        let promiseArr = []
        let _this = this
        let goodCount = 0
        let badCount = 0
        let max = this.props.selectedUsers.selectedUsers.length
        this.props.selectedUsers.selectedUsers.forEach((user, i) => {
            promiseArr.push(
                this.props.actions.deleteUser(user)
                    .then(() => {
                        goodCount++
                        if ((goodCount + badCount) == max) {
                            _this.getUsers()
                            if (badCount) {
                                swal('Error notification')
                            }
                        }
                    })
                    .catch(() => {
                        badCount++
                        if ((goodCount + badCount) == max) {
                            _this.getUsers()
                            swal('Error notification')
                        }
                    })
            )
        })
    }

    handleTextInputChange(event){ 
        const key = event.target.name
        let value = event.target.value
        if (value[0] == ' ') value = value.trim()
        this.props.actions.handleUserChange(key, value)
        this.props.actions.clearAddUserErrors(event.target.name)
    }

    onChange(e){
        this.props.actions.searchUsersQuery( e.target.value )
    }

    // this allows you to search through the users list by both fullname and email
    filterUserSearchResult(clear){
        if (clear) {
            this.props.actions.getUsers()
            return
        }
        let searchQuery = this.props.users.searchQuery.trim()
        if (searchQuery == "") return  // if the search is all spaces, don't run it
        
        this.props.actions.searchUsers( searchQuery )
            .then(() => this.props.actions.formatUsers( this.props.users.users ))
    }

    selectRadio(val1, val2){
        this.props.actions.selectUserRole(val2)
    }

    activateUser() {
        this.props.actions.activateUser()
    }

    toggleModal( modalType ){
        if (!modalType.length) modalType.preventDefault()
        switch (modalType) {
            case 'addUser':
                this.props.actions.emptyUserData()
                this.props.actions.showAddUserModal()
                break
            case 'deleteUser':
                if (this.props.selectedUsers.selectedUsers.length) {
                    this.props.actions.showDeleteUserModal()
                }
                break
            default:
                this.props.actions.hideModals()
        }
    }

    render(){

        return (
            <div>
                <ManageUsersView
                    selectedUsers={ this.props.selectedUsers.selectedUsers }
                    addUserModalVisible={ this.props.modals.addUser }
                    toggleModal={ this.toggleModal }
                    searchValue={ this.props.users.searchQuery }
                    currentFilter={ this.props.users.lastSearch }
                    filterUsers={ this.filterUserSearchResult }
                    onChange={ this.onChange }
                    usersLength={ this.props.users.users.length }
                />
                <UserModals
                    addUserModalVisible={ this.props.modals.addUser }
                    deleteUserModalVisible={ this.props.modals.deleteUser }
                    toggleModal={ this.toggleModal }
                    userLength={ this.props.selectedUsers.selectedUsers.length }
                    onChange={ this.handleTextInputChange }
                    newUserRole={ this.props.user.newUserRole }
                    newUserFirstName={ this.props.user.newUserFirstName }
                    newUserLastName={ this.props.user.newUserLastName }
                    newUserEmail={ this.props.user.newUserEmail }
                    newUserStatus={ this.props.user.newUserStatus }
                    addUser={ this.addUser } 
                    deleteUser={ this.deleteUser }
                    selectRadio={ this.selectRadio }
                    toggleActivation={ this.activateUser }
                    userActivated={ this.props.user.activated }
                    addUserErrors={ this.props.errors.addUser }
                />
            </div>
        )
    }
}

ManageUsers.propTypes = {} 

function mapStateToProps( state, ownProps ) {
    return {
        user:           state.userReducer, // singular
        users:          state.usersReducer, // plural
        selectedUsers:  state.selectedUsersReducer,
        formattedUsers: state.formatUsersReducer,
        modals:         state.modalReducer,
        errors:         state.inputErrorReducer
    }
}

function mapDispatchToProps( dispatch ) {
    return {
        actions: bindActionCreators( Object.assign( {}, modalActions, userActions ), dispatch )
    }
}

export default connect( mapStateToProps, mapDispatchToProps )( ManageUsers )