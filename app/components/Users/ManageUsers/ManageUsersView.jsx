import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-bootstrap'

import CommonSearchBox from '../../Common/CommonSearchBox'
import SearchResultInfoBar from '../../Common/SearchResultInfoBar'
import CommonTableActionIcon from '../../Common/CommonTableActionIcon'

import { FlexAlignCenter } from '../../Common/CommonStyle'

function ManageUsersView ({ selectedUsers, onChange, filterUsers, searchValue, currentFilter, toggleModal, usersLength }) {
    
    let opacity1 = selectedUsers.length == 1 ? '0.8': '0.2'
    let opacitySome = selectedUsers.length > 0 ? '0.8': '0.2'
    
    return (
        <Row>
            <SearchResultInfoBar 
                clearSearch={ filterUsers } 
                length={ usersLength } 
                query={ currentFilter } 
            />
            <Col lg={ 3 } style={ {maxWidth: "300px"} }>
                <CommonSearchBox 
                    filterItems={ filterUsers } 
                    name='searchValue' 
                    type="emailSearch" 
                    value={ searchValue } 
                    onChange={ onChange }
                /> 
            </Col>
            <Col lg={ 3 }>
                <FlexAlignCenter style={ {maxWidth: "220px", margin: "0px 15px 15px 15px"} }>
                    <CommonTableActionIcon 
                        action={ () => { toggleModal('deleteUser') } }
                        fontSize="34px"
                        iconClassName="fa fa-trash-o"
                        marginRight="15px"
                        opacity={ opacitySome }
                        title="Delete"
                    />
                    <CommonTableActionIcon 
                        action={ () => { toggleModal('addUser') } }
                        fontSize="34px"
                        iconClassName="fa fa-plus-circle"
                        title="Add"
                    />
                </FlexAlignCenter>
            </Col>
        </Row>
    )

}

ManageUsersView.propTypes = {
    currentFilter:  PropTypes.string.isRequired,
    filterUsers:    PropTypes.func.isRequired,
    searchValue:    PropTypes.string.isRequired,
    selectedUsers:  PropTypes.array.isRequired,
    toggleModal:    PropTypes.func.isRequired,
    usersLength:    PropTypes.number.isRequired,
    onChange:       PropTypes.func.isRequired,
} 


export default ManageUsersView
