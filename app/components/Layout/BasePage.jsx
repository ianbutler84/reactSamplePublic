import React from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

function BasePage({ children, location }) {

    return (
        <div className="wrapper">
            <ReactCSSTransitionGroup
                transitionName="rag-fadeIn"
                transitionEnterTimeout={ 500 }
                transitionLeaveTimeout={ 300 }
                transitionAppearTimeout={ 500 }
                transitionAppear={ true }
                transitionEnter={ true }
                transitionLeave={ true }
            >
                {
                    React.cloneElement(children, {
                        key: location.pathname
                    })
                } 
            </ReactCSSTransitionGroup>
        </div>
    )
}

export default BasePage
