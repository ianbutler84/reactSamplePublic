import React from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import Header from './Header'
import Footer from './Footer'

function Base({ location, children }) {
    const animationName = 'rag-fadeIn'

    return (
        <div className="wrapper">
            <Header />

            <section>
                <ReactCSSTransitionGroup
                    transitionName={ animationName }
                    transitionEnterTimeout={ 500 }
                    transitionAppearTimeout={ 500 }
                    transitionAppear={ true }
                    transitionLeaveTimeout={ 300 }
                    transitionEnter={ true }
                    transitionLeave={ true }
                >
                    {
                        React.cloneElement(children, {
                            key: location.pathname
                        })
                    } 
                </ReactCSSTransitionGroup>
            </section>
            <Footer /> 
        </div>
    )
}


export default Base
