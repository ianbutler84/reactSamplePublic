import React from 'react'
import { connect } from 'react-redux'

import { MainLogo } from '../Common/CommonStyle'

class Header extends React.Component {
    constructor(){
        super(props)

        this.logout = this.logout.bind(this)
    }

    determineIfLoggedIn() {
        if (localStorage.getItem('token')) {
            return (
                <a onClick={ this.logout }>{ this.props.translate('header.logout') }</a>
            )
        }
    }

    logout(){
        localStorage.removeItem('token')
        this.props.history.history.push('login')
    }

    render() {
        const { translate } = this.props
        return (
            <header className="topnavbar-wrapper">
                <nav role="navigation" className="navbar topnavbar">
                    <div className="navbar-header">
                        <div className="logoContainerContainer" style={ {color: '#fff', paddingTop: '16px', overflow: 'hidden', backgroundPositionX: '19px'} }>
                            <MainLogo backgroundPosition="-14px" className="logoContainer" />
                        </div>
                    </div>
                    <div className="nav-wrapper">
                        <ul className="nav navbar-nav">
                            <li>
                                <a href="#" data-trigger-resize="" data-toggle-state="aside-collapsed" className="hidden-xs">
                                    <em className="fa fa-navicon"></em>
                                </a>
                                <a href="#" data-toggle-state="aside-toggled" data-no-persist="true" className="visible-xs sidebar-toggle">
                                    <em className="fa fa-navicon"></em>
                                </a>
                            </li>
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            <li>{ this.determineIfLoggedIn() }</li>
                        </ul>
                    </div>
                </nav>
            </header>
        )
    }

}

function mapStateToProps( state, ownProps ) {
    return {
        history: state.historyReducer
    }
}

export default connect( mapStateToProps )( Header )