import React from 'react'
import PropTypes from 'prop-types'


const CommonModalHeader = ({ header, toggleModal }) => {

    return (
        <div style={{ padding: '15px 15px 0px 15px' }}>
            <span onClick={ toggleModal }>
                <i style={{cursor: 'pointer', fontSize: '24px'}} className='fa fa-times pull-right'></i>
            </span>
            <h4>{ header }</h4>
        </div>
    )
}

CommonModalHeader.propTypes = {
    header:     PropTypes.string.isRequired,
    toggleModal:  PropTypes.func.isRequired
}

export default CommonModalHeader