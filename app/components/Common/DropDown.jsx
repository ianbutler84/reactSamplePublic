import React from 'react'
import PropTypes from 'prop-types'
import { Dropdown, MenuItem } from 'react-bootstrap'

/**
 * 
 * titleValue will be a string which will match the key you want displayed
 * 
 * If you want to have functions/actions at the bottom of the list create a body2 arr
 * It should consist of the following:
 *      body2: [{titleValue: "Whatever you want displayed", func: "whateverFuncYouWantExecuted()"}]
 * 
 */

function CommonDropdown ({ translate, title, disabled, titleValue, id, body, body2, selectItem, style, className, noCaret }) {
    let titleString
    if (typeof title != 'string') {
        title.props.children.forEach((child) => {
            if (typeof child == 'string') titleString = child
        })
        if (!titleString) titleString = ''
    }
    
    titleString = title
    if (translate(title).indexOf('localized key') < 0) {    
        titleString = translate( title )
    }

    return (
        <Dropdown id={ id } disabled={ disabled || false }>
            <Dropdown.Toggle className={ className || '' } noCaret={ noCaret || false } style={ style }>
                { titleString }
            </Dropdown.Toggle>
            <Dropdown.Menu style={ { right: '0px', left: 'inherit', 'maxHeight': '400px', 'overflowY': 'auto' } }>
                { 
                    body.map( (key, i) => {
                        if (key.groupID) {
                            return <MenuItem onClick={ () => selectItem( key ) } key={ i }>{ key[titleValue] }</MenuItem>
                        } else {
                            return <MenuItem onClick={ () => selectItem( key ) } key={ i }>{ translate(key[titleValue]) }</MenuItem>
                        }
                    }) 
                }
                {
                    body2 && <MenuItem style={ {width: '90%', margin: '0px auto'} } divider />
                }
                {
                    body2 && body2.map( (key, i) => {
                        return <MenuItem onClick={ key.func } key={ i }>{ translate(key[titleValue]) }</MenuItem>
                    })
                }       
            </Dropdown.Menu>
        </Dropdown>
    )
}

CommonDropdown.propTypes = {
    //title: PropTypes.string.isRequired,
    titleValue: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    body: PropTypes.array.isRequired,
    body2: PropTypes.array,
    selectItem: PropTypes.func
}

export default CommonDropdown