import React from 'react'
import PropTypes from 'prop-types'
import { Modal } from 'react-bootstrap'

import CommonModalHeader from './CommonModalHeader'

const CommonModal = ({ titleForModal, component, modalVisible, toggleModal, modalCustomClass }) => {

    return (
        <Modal dialogClassName={modalCustomClass} show={ modalVisible } onHide={ () => { toggleModal() } }>
            <CommonModalHeader header={ titleForModal } toggleModal={ toggleModal } />
            <Modal.Body>
                { component } 
            </Modal.Body>
        </Modal>
    )
}

CommonModal.propTypes = {
    titleForModal:  PropTypes.string.isRequired,
    component:      PropTypes.object.isRequired,
    toggleModal:    PropTypes.func.isRequired,
    modalVisible:   PropTypes.bool.isRequired
}

export default CommonModal