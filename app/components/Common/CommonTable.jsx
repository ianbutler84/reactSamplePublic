import React from 'react'
import PropTypes from 'prop-types'
import { Table } from 'react-bootstrap'

import TableBodyRow from './TableBodyRow2'
import TableHeaderRow from './TableHeaderRow'

const CommonTable = ({ translate, headings, body, selectAll, allItemsSelected, deleteItem, checkItem, children, sorter, sortParam, sortDirection, selectFilter }) => {
    
    return (
        <Table style={ selectFilter && {minHeight: '190px'}} responsive striped bordered hover>
            <thead>
                <TableHeaderRow 
                    headings={ headings } 
                    allItemsSelected={ allItemsSelected } 
                    selectAll={ selectAll } 
                    sortDirection={ sortDirection }
                    sortParam={ sortParam }
                    selectFilter={ selectFilter }
                    translate={ translate }
                    sorter={ sorter } />
            </thead>
            <tbody>
                {
                    body.map((item, i) => {
                        return (
                            <TableBodyRow 
                                deleteItem={deleteItem} 
                                checkItem={ checkItem } 
                                item={ item } 
                                i={ i } 
                                key={i} 
                            >
                                { children }
                            </TableBodyRow>
                        )
                    })                                     
                }
                {
                    !body.length && 
                    <tr>
                        {
                            headings.map((item, i) => {
                                if (i == 0) return <td key={i}>No data</td>
                                else return <td key={i}></td>
                            })
                        }
                    </tr>
                }
            </tbody>
        </Table>
    )
}

CommonTable.propTypes = {
    headings:   PropTypes.array.isRequired,
    body:       PropTypes.array.isRequired,
    selectAll:  PropTypes.func,
    sorter:     PropTypes.func
}

export default CommonTable