import React from 'react';
import PropTypes from 'prop-types'
import { Pagination } from 'react-bootstrap'

const Paginator = ({ length, handlePageSelect, activePage, amountShown }) => {
    return (
        <Pagination
            style={{margin: '0px'}}
            className={(length <= amountShown ? 'hidden' : 'shown') + ' pull-right'}
            prev
            next 
            last
            first
            ellipsis
            maxButtons={ 3 }
            items={ Math.ceil( length / amountShown )  }
            activePage={ activePage }
            onSelect={ handlePageSelect }
        >
        </Pagination>
    )
}

Paginator.propTypes = {
    length: PropTypes.number.isRequired,
    handlePageSelect: PropTypes.func.isRequired,
    activePage: PropTypes.number.isRequired,
    amountShown: PropTypes.number.isRequired
}

export default Paginator