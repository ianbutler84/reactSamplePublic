import React from 'react'
import PropTypes from 'prop-types'

class TextInput extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            maxLength: 256,
            labelElement: ''
        }
        this.checkCharacterType = this.checkCharacterType.bind(this)
    }

    componentWillMount() {
        this.useLabel()
        this.setLimits()
    }

    useLabel() {
        let labelElement
        if (this.props.label) { labelElement = <label className='text-muted'>{this.props.label}</label> }
        else { labelElement = '' }
        this.setState({ labelElement })
    }

    setLimits() {
        let maxLength
        if (this.props.type == 'text') maxLength = FIXEDVALUES.maxTextLimitFirstName
        else if (this.props.type == 'name') maxLength = FIXEDVALUES.maxTextLimitFirstName
        else if (this.props.type == 'version') maxLength = FIXEDVALUES.maxTextLimitFirstName
        else if (this.props.type == 'email') maxLength = FIXEDVALUES.maxTextLimitEmail
        else if (this.props.type == 'password') maxLength = FIXEDVALUES.maxTextLimitPassword
        this.setState({ maxLength })
    }

    checkCharacterType( e ) {
        if (e.target.value.trim() == '') e.target.value = ''
        if (this.props.type == 'text') {
            if ( e.target.value.search(/[\]/[/{\}!@#$%^`~&|'"\\;:?><,.*/(/)_+\-=]/) < 0 ) this.props.onChange(e)  
        }
        else if (this.props.type == 'name') {
            if ( e.target.value.search(/[\]/[/{\}!@#$%^`~&|'"\\;:?><,.*/(/)_+\-=0-9]/) < 0 ) this.props.onChange(e)  
        }
        else if (this.props.type == 'version') {
            if ( e.target.value.search(/[\]/[/{\}!@#$%^`~&|'"\\;:?><,*/(/)_+\-=]/) < 0 ) this.props.onChange(e)  
        }
        else if (this.props.type == 'emailSearch') this.props.onChange(e)  
        else if (this.props.type == 'password') this.props.onChange(e)
        else if (this.props.type == 'email') this.props.onChange(e)
    }

    render() {
        const { value, type, placeholder, name, icon, style, children } = this.props
        
        if (style) style.position = 'relative'

        return (
            <div style={ style || {position: 'relative'} } className="form-group has-feedback">
                { this.state.labelElement }    
                <input
                    className="form-control"
                    maxLength={ this.state.maxLength } 
                    name={ name }
                    placeholder={ placeholder }
                    type={ type }
                    value={ value }
                    onBlur={ this.onBlur }
                    onChange={ this.checkCharacterType }
                />
                <span className={ 'fa form-control-feedback text-muted ' + icon }></span>
                { children }
            </div>
        )
    }
}

TextInput.propTypes = {
    type: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    icon: PropTypes.string,
    label: PropTypes.string,
    name: PropTypes.string,
    placeholder: PropTypes.string,
    style: PropTypes.object
}

export default TextInput