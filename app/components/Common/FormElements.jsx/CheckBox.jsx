import React from 'react'
import PropTypes from 'prop-types'

import { StyledCheckBox } from '../CommonStyle'

const CheckBox = ({ onClick, checked, onChange }) => {

    return (
        <StyledCheckBox 
            className={ checked ? 'checked' : '' }
            onClick={ onClick } 
        >
            <i className='fa fa-check'></i>
            <input 
                checked={ checked }
                onChange={ onChange }
                style={{height: '15px'}} 
                type='checkbox' 
                className='form-control' />
        </StyledCheckBox>
    )
}

CheckBox.propTypes = {
    checked: PropTypes.bool,
    onChange: PropTypes.func,
    onClick: PropTypes.func
}

export default CheckBox