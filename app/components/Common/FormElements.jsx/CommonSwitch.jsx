import React from 'react'
import PropTypes from 'prop-types'

import { FlexAlignCenter, Switch } from '../CommonStyle'

const CommonSwitch = ({ title, customColor, value, toggler }) => {

    return (
        <FlexAlignCenter>
            <span style={ { paddingRight: '15px' } }>{ title }</span>
            <Switch customColor={ customColor }>
                <input onChange={ toggler } value={ value } type='checkbox'/>
                <span className='slider round'></span>
            </Switch>
        </FlexAlignCenter>
    )
}

CommonSwitch.propTypes = {
    title: PropTypes.string.isRequired,
    customColor: PropTypes.string
}

export default CommonSwitch