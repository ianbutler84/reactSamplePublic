import React from 'react'
import PropTypes from 'prop-types'

import { StyledRadio } from '../CommonStyle'

const Radio = ({ name, checkedBool, value, onClick }) => {

    return (
        <StyledRadio onClick={ () => { onClick(name, value) } } >
            <span className={ checkedBool == value ? 'checked' : '' }></span>
            <input 
                onChange={ () => {} }
                name={ name }
                checked={ checkedBool == value }
                value={ value }
                type="radio"
            />
        </StyledRadio>
    )
}

Radio.propTypes = {

}

export default Radio