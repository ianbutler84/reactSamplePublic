import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

class CommonLink extends React.Component {
    constructor(props) {
        super(props)

        this.changeRoute = this.changeRoute.bind(this)
    }

    changeRoute() {
        this.props.history.history.push( this.props.to )
    }
    
    render() {
        const { title, children, className } = this.props
        if (title) {
            return (
                <a className={ className || '' } title={ title } onClick={ this.changeRoute }>
                    { children }
                </a>
            )
        } else {
            return (
                <a className={ className || '' } onClick={ this.changeRoute }>
                    { children }
                </a>
            )
        }
    }
}

function mapStateToProps( state, ownProps ) {
    return {
        history: state.historyReducer
    }
}

export default connect( mapStateToProps )( CommonLink )