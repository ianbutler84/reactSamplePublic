
import React from 'react'
import PropTypes from 'prop-types'
import { Dropdown, MenuItem } from 'react-bootstrap'

const TableFilter = ({ translate, header, selectFilter }) => {
    
    return (
        <Dropdown id={ header.name }>
            <Dropdown.Toggle noCaret={ true } style={ {backgroundColor: '#fff !important',border: 'none', padding: '0px'} }>
                <a style={ {fontWeight: 'bold'} }>
                    { translate(`${header.current}`) }
                    <i style={ {paddingLeft: '5px'} } className='fa fa-caret-down'></i>
                </a>
            </Dropdown.Toggle>

            <Dropdown.Menu style={ { right: '0px', left: 'inherit'} }>
                <MenuItem header>{ translate('users.table.showdisplay') }</MenuItem>
                {
                    header.options.map( ( key, i ) => {
                        return (
                            <MenuItem key={ i } onClick={ () => selectFilter(key, header.name) } >
                                <span style={ { paddingLeft: '10px'} }>
                                    { translate(`${key}`) }
                                </span>
                            </MenuItem>
                        )
                    })
                }
            </Dropdown.Menu>
        </Dropdown>
    )
}

TableFilter.propTypes = {
    header: PropTypes.object.isRequired,
    selectFilter: PropTypes.func
}

export default TableFilter