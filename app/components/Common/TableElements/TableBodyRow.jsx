import React from 'react'
import PropTypes from 'prop-types'

import CheckBox from '../FormElements/CheckBox'
import CustomTableData from './CustomTableData'

import { Truncate } from '../CommonStyle.jsx'

const TableBodyRow = ({ item, i, checkItem, deleteItem, children }) => {
    return (
        <tr key={ i }>
            {
                Object.keys(item).map((key2, j) => {
                    // Allows custom display to be added and injects data
                    if (item[key2].custom == 'customComponent') return <CustomTableData key={ j } component={ children[item[key2].name] } customData={ item[key2].data } />
                    // Ignores data.  Data might be sent down as a reference to original data state since data must be formatted to enter the table
                    else if (key2 == 'originalData' || key2 == 'selected') return
                    // Adds a checkbox
                    else if (key2 == 'checkbox') {
                        return (
                            <td key={ j }>
                                <div style={{width: '15px', marginBottom: '0px'}} className="form-group">
                                    <CheckBox 
                                        checked={ item.selected }
                                        type="checkbox" 
                                        onClick={() => checkItem(item) } 
                                    />
                                </div>
                            </td>
                        )
                    }
                    // Standard display of text
                    else {
                        return (
                            <td style={{maxWidth: '200px'}} key={ j }>
                                <Truncate title={ item[key2] }>
                                    { item[key2] }
                                </Truncate>
                            </td>
                        )
                    }
                })
            }
        </tr>
    )
}

TableBodyRow.propTypes = {
    item: PropTypes.object.isRequired,
    i: PropTypes.number.isRequired,
    deleteItem: PropTypes.func
}

export default TableBodyRow