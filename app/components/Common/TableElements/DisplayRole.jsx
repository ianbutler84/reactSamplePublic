import React from 'react'
import PropTypes from 'prop-types'

import { Truncate } from '../../CommonStyle'

const DisplayRole = ({ customData }) => {

    let roleName 

    customData["data-original-data"].forEach((data) => {
        if ('roles' in data) {
            data.roles.forEach((role) => {
                if (role == 'admin' || role == 'cvsguest' ) roleName = role[0].toUpperCase() + role.slice(1)     
            })
        }
    })

    return (
        <td style={{maxWidth: '200px'}}>
            <Truncate title={ customData.value }>
                { roleName || 'Customer' } 
            </Truncate>
        </td>
    )
}

export default DisplayRole