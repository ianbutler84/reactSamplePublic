import React from 'react';
import PropTypes from 'prop-types'

import CheckBox from '../FormElements/CheckBox'
import TableHeader from './TableHeader'

import { Truncate } from '../CommonStyle.jsx'


const TableHeaderRow = ({ translate, headings, allItemsSelected, selectAll, sorter, sortDirection, sortParam, selectFilter }) => {
    
    return (
        <tr>
            {
                headings.map((header, i) => {
                    if (header == 'checkbox') {
                        return (
                            <th key={ i }>
                                <div className="form-group" style={{width: '15px', marginBottom: '0px'}}>
                                    <CheckBox checked={ allItemsSelected } onClick={ selectAll } />
                                </div>
                            </th>
                        )
                    }
                    if (typeof header == 'object') {
                        return (
                            <TableHeader 
                                sortParam={ sortParam }
                                sortDirection={ sortDirection }
                                sorter={ sorter } 
                                header={ header } 
                                selectFilter={ selectFilter }
                                translate={ translate }
                                key={ i } />
                        )
                    } 
                    return (
                        <th style={{maxWidth: '200px'}} key={ i }>
                            <Truncate title={ header }>
                                { header }
                            </Truncate>
                        </th>
                    )
                })
            }
        </tr>
    )
}

TableHeaderRow.propTypes = {
    headings:           PropTypes.array, 
    allItemsSelected:   PropTypes.bool, 
    selectAll:          PropTypes.func,
    sorter:             PropTypes.func
}

export default TableHeaderRow