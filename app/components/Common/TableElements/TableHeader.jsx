
import React from 'react'
import PropTypes from 'prop-types'

import TableFilter from './TableFilter'

import { FlexAlignCenterSpBtwn, ShowIconsOnHover } from '../CommonStyle.jsx'

const TableHeader = ({ translate, header, sorter, sortParam, sortDirection, selectFilter }) => {
    
    if ( sortParam == header.name ) {
        return (
            <th style={ {verticalAlign: 'top'} } onClick={ () => sorter( header.name ) }>
                <span title={ translate(header.name) } style={ {whiteSpace: 'nowrap', paddingRight: '5px'} }>
                    { translate(header.name) }
                </span>
                <span style={ {whiteSpace: 'nowrap', width: '20px'} }>
                    { sortDirection == 'down' && <i className="fa fa-long-arrow-down text-info"></i> }
                    { sortDirection == 'up' && <i className="fa fa-long-arrow-up text-info"></i> }
                </span>
            </th>
        )
    } else if ( header.sortable ) {
        return (
            <th style={ {verticalAlign: 'top'} } onClick={ () => sorter( header.name ) }>
                <ShowIconsOnHover>
                    <span title={ translate(header.name) } style={ {whiteSpace: 'nowrap', paddingRight: '5px'} }>
                        { translate(header.name) }
                    </span>
                    <span style={ {whiteSpace: 'nowrap', width: '20px'} }>
                        <i className="fa fa-long-arrow-down text-info"></i>
                        <i className="fa fa-long-arrow-up text-info"></i>                    
                    </span>
                </ShowIconsOnHover>
            </th>
        )
    } else if ( header.filterable ) {

        return (
            <th style={ {verticalAlign: 'top'} }>
                <FlexAlignCenterSpBtwn>
                    <span style={ { whiteSpace: 'nowrap', paddingRight: '5px'} } title={ translate(header.name) }>
                        { translate(`${header.name}`) }
                    </span>
                    <TableFilter 
                        header={ header }
                        selectFilter={ selectFilter }
                        translate={ translate }
                    />
                </FlexAlignCenterSpBtwn>
            </th>
        )
    }
}

TableHeader.propTypes = {
    header: PropTypes.object.isRequired,
    sorter: PropTypes.func.isRequired, 
    selectFilter: PropTypes.func,
    sortDirection: PropTypes.string,
    sortParam: PropTypes.string
}

export default TableHeader