import React from 'react'
import PropTypes from 'prop-types'

const CustomTableData = (props) => {
    const { component, customData } = props
    let Component

    if (component.props.onClick) Component = React.cloneElement(component, {onClick:( ) => { component.props.onClick(customData) }} )
    else Component = React.cloneElement(component, { customData } )

    return (          
        Component 
    )
}

CustomTableData.propTypes = {
    component: PropTypes.object,
    customData: PropTypes.object
}

export default CustomTableData