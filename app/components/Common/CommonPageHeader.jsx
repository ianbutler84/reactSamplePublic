import React from 'react'
import PropTypes from 'prop-types'


const CommonPageHeader = ({ title, description }) => {

    return (
        <div className='jumbotron' style={{height: '100px', padding: description ? '25px': '30px 20px', margin: '-21px', marginBottom: '20px'}}>
            <h2 style={{margin: '0px'}}>{ title }</h2>
            { description && <p style={{ marginBottom: '0px', fontSize: '13px', color: 'rgb(185, 185, 185)' }}>{ description }</p>}
        </div>
    )
}

CommonPageHeader.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string
}

export default CommonPageHeader