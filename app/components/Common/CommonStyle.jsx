import styled from 'styled-components'


/******************************************************************/

// Styles using styled-components... instead of CSS or inline styles
// documentation on this is on https://www.styled-components.com/docs

/******************************************************************/

// used for cutting off long words/ file names
const Truncate = styled.div`
    width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`

const StyledDetails = styled.div`
    width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    i {
        display: none;
    }
    input {
        display: none;
    }
`

// Used as a checkbox instead of default checkboxes
const StyledCheckBox = styled.div`
    width: 18px;
    height: 18px;
    border: 1px solid #23b7e5;
    border-radius: 3px;
    i {
        display: none;
    }
    &.checked {
        background: #23b7e5;
        position: relative;
        i { 
            color: #fff;
            display: inline;
            font-size: 14px;
            position: absolute;
            top: 1px;
            left: 1px;
        }
    }
    input {
        display: none;
    }
`

// Used as a radio button instead of the default radio button
const StyledRadio = styled.div`
    border: 1px solid #23b7e5;
    height: 20px;
    width: 20px;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    .checked {
        background-color: #23b7e5;
        border-radius: 50%;
        height: 12px;
        width: 12px;
    }
    input {
        display: none;
    }
`

// wraps the buttons at the bottom of a modal
const ModalBottomButtonGroup = styled.div`
    margin: 15px -15px -15px -15px; 
    border-bottom-left-radius: 6px;
    border-bottom-right-radius: 6px;
    padding: 10px 15px;
`

// various flexbox combinations
const FlexAlignCenter = styled.div`
    display: flex;
    align-items: center;
`
const FlexJustAlignCenter = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`
const FlexColumnAlignCenter = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`
const FlexJustStart = styled.div`
    display: flex;
    justify-content: flex-start;
`
const FlexJustEnd = styled.div`
    display: flex;
    justify-content: flex-end;
`
const FlexAlignCenterSpBtwn = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`

const FlexCenteredInfo = styled.div`
    display: flex;
    .headingInfo {
        text-align: end;
        flex: 1;
        font-weight: bold;
        margin-right: 5px;
    }
    .bodyInfo {
        flex: 1;
        margin-left: 5px;
    }
`

const ShowIconsOnHover = styled.span`
    i { 
        opacity: 0;
    }
    &:hover i {
        opacity: 0.5;
    }
`

const Switch = styled.label`
    position: relative;
    display: inline-block;
    width: 48px;
    height: 22px;
    margin-bottom: 0px;
    input { display: none; }
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        transition: .4s;
    }
    .slider:before {
        position: absolute;
        content: "";
        height: 16px;
        width: 16px;
        left: 3px;
        right: 3px;
        top: 3px;
        bottom: 3px;
        background-color: #fff;
        transition: .4s;
    }
    input:checked + .slider { background-color: ${(props) => props.customColor ? props.customColor : '#2196F3'}; }
    input:focus + .slider { box-shadow: 0 0 1px ${(props) => props.customColor ? props.customColor : '#2196F3'}; }
    input:checked + .slider:before { transform: translateX(26PX); }
    .slider.round { border-radius: 34px; }
    .slider.round:before { border-radius: 50%; }
`

const MainLogo = styled.div`
    background: url(img/mainLogo.png) no-repeat;
    width: 220px;
    height: 40px;
    background-position-y: ${(props) => props.backgroundPosition }
`

export {
    Truncate,
    StyledDetails,
    StyledCheckBox,
    StyledRadio,
    ModalBottomButtonGroup,
    FlexAlignCenter,
    FlexJustAlignCenter,
    FlexColumnAlignCenter,
    FlexJustStart,
    FlexJustEnd,
    FlexAlignCenterSpBtwn,
    StatusNote,
    FlexCenteredInfo,
    ShowIconsOnHover,
    Switch,
    MainLogo
}