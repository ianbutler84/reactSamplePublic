import React from 'react'
import { Router, Route, Redirect } from 'react-router'

// Base page components
import Base from './components/Layout/Base'
import BasePage from './components/Layout/BasePage'

import NotFound from './components/Pages/NotFound'

import UsersPage from './components/Users/UsersPage'
import RedirectPage from './components/Redirect/RedirectPage'
import { RequireAuth } from './components/Auth/RequireAuth'

import customHistory from './config/historyConfig'


class Routes extends React.Component {
    componentDidMount(){
        axiosConfig()
    }
    
    render() {
        return (
            <Router history={ customHistory }>
                <Redirect from="/" to="/login" />
                
                <Route path="/" component={ Base }>
                    <Route path="users" component={ RequireAuth(UsersPage) } />
                </Route>

                <Route path="/" component={ BasePage }>
                    <Route path="redirect" component={ RedirectPage } />
                </Route>

                <Route path="*" component={ NotFound }/>
            </Router>
        )
    }
}

export default Routes