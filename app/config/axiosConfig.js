import axios from 'axios'
import { hashHistory } from 'react-router'

export default () => {
    axios.interceptors.request.use((config) => {
        config.headers['Csrf-Token'] = 'nocheck'
        config.headers['Content-Type'] = 'application/json'
        config.headers['Accept'] = 'application/json'
        if (!!window.MSInputMethodContext && !!document.documentMode) {
            config.headers['Pragma'] = 'no-cache'   // neccessary for stopping ie11 from caching ajax call results
        }
        let token = localStorage.getItem('token')
        if (token) {
            config.headers['X-Auth-Token'] = token
        }
        
        return config
    })

    axios.interceptors.response.use((response) => {
        return response
    }, (err) => {
        if (err.response.status == 401) {
            localStorage.removeItem('token')
            hashHistory.push('login')
            return Promise.reject(err)
        } 
        else return Promise.reject(err)
    })
}