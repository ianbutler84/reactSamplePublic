import React from 'react'
import customHistory from '../../config/historyConfig'

export function RequireAuth(Component) {

    return ( 
        class AuthenticatedComponent extends React.Component {
            constructor(props) {
                super(props)

                this.checkToken()
            }

            checkToken() {
                if (!localStorage.getItem('token')) {
                    customHistory.push('/login')
                }
            }
            
            render() {
                return (
                    <Component />
                )
            }
        }
    )
}