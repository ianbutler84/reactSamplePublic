import * as types from './actionTypes'


export function showAddUserModal() {
    return { type: types.SHOW_ADD_USER_MODAL }
}

export function showDeleteUserModal() {
    return { type: types.SHOW_DELETE_USER_MODAL }
}

export function showDeleteNotificationModal( notification ) {
    return { type: types.SHOW_DELETE_NOTIF_MODAL, notification }
}

export function hideModals() {
    return { type: types.HIDE_MODALS }
}