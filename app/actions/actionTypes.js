
// User/Users
export const GET_USERS_SUCCESS =            'GET_USERS_SUCCESS'
export const FORMAT_USERS =                 'FORMAT_USERS'
export const ADD_USER_SUCCESS =             'ADD_USER_SUCCESS'
export const DELETE_USER_SUCCESS =          'DELETE_USER_SUCCESS'
export const POPULATE_USER_DATA =           'POPULATE_USER_DATA'
export const EMPTY_USER_DATA =              'EMPTY_USER_DATA'

// Modals
export const SHOW_ADD_USER_MODAL =          'SHOW_ADD_USER_MODAL' 
export const SHOW_DELETE_USER_MODAL =       'SHOW_DELETE_USER_MODAL' 
export const SHOW_DELETE_NOTIF_MODAL =      'SHOW_DELETE_NOTIF_MODAL'
export const HIDE_MODALS =                  'HIDE_MODALS'