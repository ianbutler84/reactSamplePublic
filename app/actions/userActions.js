import axios from 'axios'

import * as types from './actionTypes'
// Action Creators (Redux)
//  Creates actions 
// MUST have a type property

function getUsersSuccess(users) {
    return { type: types.GET_USERS_SUCCESS, users:users }
}

export function getUsers() {
    return function(dispatch) {
        return axios.get(`${someApi}/users`)
            .then((data) => { return dispatch(getUsersSuccess(data.data)) })
            .then((data) => { return dispatch( formatUsers(data.users) )})
            .catch( (err) => { throw( err ) })
    }
}

function addUserSuccess() {
    return { type: types.ADD_USER_SUCCESS }
}

export function addUser(user) {
    let orgId = localStorage.getItem('orgID')
    return function( dispatch ) {
        let userRole = user.newUserRole.toLowerCase() == 'customer' ? 'member' : user.newUserRole

        if (userRole == 'admin') return addUserWithRole(user, userRole, orgId, dispatch)
        else {
            return getMemberRoleId( orgId )
                .then((data) => {
                    return addUserWithRole(user, data, orgId, dispatch)
                })
        }
    }
}

function getMemberRoleId() {
    let orgId = localStorage.getItem('orgID')
    return axios.get(`${apiUrl}/o/${orgId}/roles`)
        .then((data) => {
            let roleId 
            data.data.forEach((roleItem) => {
                if ('base' in roleItem && roleItem.base == 'member') roleId = roleItem.roleID   
            })
            return roleId
        })
}

function addUserWithRole(user, userRole, orgIddddd, dispatch) {
    let orgId = localStorage.getItem('orgID')
    return axios.post(`${apiUrl}/o/${orgId}/users/status/registered`, {
        firstName: user.newUserFirstName.trim(),
        lastName: user.newUserLastName.trim(),
        email: user.newUserEmail.trim(),
        roles: [ userRole ]
    })
        .then(() => { return dispatch(addUserSuccess())})
        .then(() => { return dispatch( getUsers(orgId) )})
        .catch((err) => { throw( err )})
}

function editUserSuccess() {
    return { type: types.EDIT_USER_SUCCESS }
}

export function editUser(user) {
    let orgId = localStorage.getItem('orgID')
    let activated = user.activated == true ? 'ACTIVE' : user.newUserStatus
    let userRole = user.newUserRole.toLowerCase() != 'admin' ? 'member' : user.newUserRole

    return function( dispatch ) {
        if (userRole == 'admin') return editUserWithRole(user, userRole, activated, orgId, dispatch)
        else {
            return getMemberRoleId( orgId )
                .then((data) => {
                    return editUserWithRole(user, data, activated, orgId, dispatch)
                })
        }
        
    }
}

function editUserWithRole(user, userRole, activated, orgIdddd, dispatch) {
    let orgId = localStorage.getItem('orgID')
    return axios.put(`${apiUrl}/o/${orgId}/users/${user.newUserId}`, {
        profile: {
            'firstName': user.newUserFirstName.trim(),
            'lastName': user.newUserLastName.trim()
        },
        role: userRole,
        status: activated
    })
        .then(() => { return dispatch( editUserSuccess() )})
        .then(() => { return dispatch( getUsers(orgId) )})
        .catch((err) => { throw(err) })
}

function deleteUserSuccess() {
    return { type: types.DELETE_USER_SUCCESS }
}

export function deleteUser(user) {
    return function( dispatch ) {
        return axios.delete( `${apiUrl}/o/${user.originalData.orgID}/user/${user.originalData.userID}` )
            .then( () => { return dispatch(  deleteUserSuccess() ) })
            .catch( (err) => { throw( err ) })
    }
}

function resetPasswordSuccess() {
    return { type: types.RESET_USER_PASSWORD_SUCCESS }
}

export function resetPassword( user ) {
    return function( dispatch ) {
        return axios.post(`${apiUrl}/auth/resets`, {
            userEmail: user.email
        })
            .then(() => { return dispatch( resetPasswordSuccess() )})
            .catch( (err) => { throw( err ) })
    }
}

export function formatUsers(users) {
    return { type: types.FORMAT_USERS, users }
}

export function selectUser(user) {
    return { type: types.SELECT_USER, user }
}

export function selectAllUsers(allSelected, users, activePage, amountShown) {
    return { type: types.SELECT_ALL_USERS, allSelected, users, activePage, amountShown }
}

export function handleUserPageSelect(newPage) {
    return { type: types.HANDLE_USER_PAGE_SELECT, newPage }
}

export function populateUserData(user) {
    return { type: types.POPULATE_USER_DATA, user }
}

export function activateUser() {
    return { type: types.ACTIVATE_USER }
}

export function emptyUserData() {
    return { type: types.EMPTY_USER_DATA }
}

export function handleUserChange(userChangeKey, userChangeValue) {
    return { type: types.HANDLE_USER_CHANGE, key: userChangeKey, value: userChangeValue }
}

export function searchUsersQuery( value ) {
    return { type: types.SEARCH_USERS_QUERY, value }
}

export function searchUsers( orgIddddddD, filter ) {
    let orgID = localStorage.getItem('orgID')
    return function( dispatch ) {
        return axios.get( `${ apiUrl }/o/${orgID}/users?filter=${filter}` )
            .then((data) => { return dispatch( searchUsersSuccess( data.data, filter ) )})
            .then((data) => { return dispatch( formatUsers(data.users))})
            .catch( (err) => {
                throw( err )
            })
    }
}

function searchUsersSuccess( users, query ) {
    return { type: types.SEARCH_USERS_SUCCESS, users, query }
}

export function sortUsersTable( param ) {
    return { type: types.SORT_USER_TABLE, param }
}

export function filterUsers( users, filterName, filterValue ) {
    return { type: types.FILTER_USERS, users, filterValue, filterName }
}

export function selectUserRole(role) {
    return { type: types.SELECT_USER_ROLE, role }
}

export function validateAddUser( addUser ) {
    return { type: types.ADD_USER_ERROR, addUser }
}

export function validateEditUser( editUser ) {
    return { type: types.EDIT_USER_ERROR, editUser }
}

export function clearAddUserErrors( name ) {
    return { type: types.CLEAR_ADD_USER_ERRORS, name }
}

export function clearEditUserErrors( name ) {
    return { type: types.CLEAR_EDIT_USER_ERRORS, name }
}

export function clearUserState() {
    return { type: types.CLEAR_USER_STATE }
}

export function initUserState( translate ) {
    return { type: types.INITIALIZE_USER_INITIAL_STATE, translate }
}