if (!Object.assign) console.log('no object assign', ASSIGN)
if (!Array.prototype.find) console.log('no find on prototype', FIND)

import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import configureStore from './store/configureStore'
import Routes from './Routes'
import AxiosConfig from './config/axiosConfig'
import DevTools from './components/DevTools/DevTools'

// Application Styles
import './styles/bootstrap.scss'
import './styles/app.scss'

AxiosConfig()   // initializes the AJAX interceptor
initializeApp()

function initializeApp() {
    const store = configureStore()
    
    if (module.hot) {
        ReactDOM.render(
            <Provider store={ store } >
                <div style={ {height: '100%'} }>
                    <Routes />
                    <DevTools store={ store } />
                </div>
            </Provider>,
            document.getElementById('app')
        )
    } else {
        ReactDOM.render(
            <Provider store={ store } >
                <Routes />
            </Provider>,
            document.getElementById('app')
        )
    }
}
